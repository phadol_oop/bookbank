/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.bookbank;

/**
 *
 * @author Black Dragon
 */
public class BookBank {
    String name;
    double balance;
    //Constructor
    BookBank(String name, double money){
        this.name = name;
        this.balance = money;
    }
    
    void deposit(double money){
        if(money <= 0){
            System.out.println("Money must have more than 0");
            return;
        }
        this.balance += money;
    }
    
    void withdraw(double money){
//        if(this.balance >= money){
//            this.balance -= money;
//        }else{
//            System.out.println("Not enough money");
//        }
        if(money <= 0){
            System.out.println("Money must have more than 0");
            return;
        }
        if(money > this.balance){
            System.out.println("Not enough money.");
            return;
        }
        this.balance -= money;
    }
}
